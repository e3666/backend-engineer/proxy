# Sumário

- [**Proxy**](#proxy)
- [**Reverse Proxy**](#reverse-proxy)


# Proxy

Proxy é um software, que faz o intermédio da request entre um server e client. Ou seja, quando fazemos uma request para um servidor, ela passa primeiro pela proxy, a informação é analisada e enviada para o servidor.

Considerando que ela está no meio do caminho, então podemos bloquear acessos por meio dela, podemos fazer cache de informações.

# Reverse Proxy

O client envia a request para uma proxy e a proxy irá escolher dentre n servidores qual utilizar, ou seja, é como um load balancer.